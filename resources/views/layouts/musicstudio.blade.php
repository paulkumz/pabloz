<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<head>
    @include('layouts.partials.head')

</head>
 

<body>

     <div id="main-wrapper"> 
   @include('layouts.partials.navigation')
 

  
      @include('layouts.partials.content1')
   
    </div>


     @include('layouts.partials.footer')
</body>
 
</html>
