
      <div id="newsdetails" class="container-fluid">            
            <div class="section">
                <div class="row">
                    <div class="col-sm-9">
                        <div id="site-content" class="site-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="left-content">
                                        <h1 class="section-title title" style="text-align: center;">Producer</h1>
                                        <div class="details-news">                                          
                                            <div class="post">
                                                <div class="entry-header">
                                                    <div class="entry-thumbnail">
                                                        <img class="img-responsive" src="images/slider/18.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="post-content">                              
                                                    <div class="entry-meta">
                                                       
                                                    </div>
                                                    <h2 class="entry-title" style="text-align: center;">
                                                       What we are doing
                                                    </h2>
                                                    <div class="entry-content">
                                                        <p style="margin-left: 50px;">We support songwriters, composers and musicians.
From small projects involving vocal overdubs and instrument tracking to large live sessions involving rhythm, string, wind and brass sections, Pabloz caters to all needs.
Our studio is fully equipped with a ……. console, great outboard, two large live rooms, a vocal booth a large control room, and a wide range of vintage and modern instruments and gear.
 </p>
                                                        <p style="margin-left: 50px;">Pabloz Recording Studio also offers high end mixing and mastering services for Musicians of all styles and genres who desire a professional finesse to their singles and albums.
Every mix requires a different approach and our team will be on hand to provide professional and diverse mixing services. Our team will work closely with each artist to get the project to its highest potential.
</p>
                                                            
                                                       
                                                        
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div><!--/post--> 
                                        </div><!--/.section-->
                                    </div><!--/.left-content-->
                                </div>
                            </div>
                        </div><!--/#site-content-->
                        
                        <div class="section related-news-section">
                            <h1 class="section-title" style="text-align:center;">Our other services</h1>
                            <div class="related-news">
                                <div id="related-news-carousel">
                                    <div class="post medium-post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/3.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            
                                            <h2 class="entry-title">
                                                <a href="news-details.html">Visual Studio</a>
                                            </h2>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post medium-post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/1.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            
                                            <h2 class="entry-title">
                                                <a href="news-details.html">VIP Lounge</a>
                                            </h2>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post medium-post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"> <iframe width="640" height="360" src="https://www.youtube.com/embed/csg3s6Ob4uc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                           
                                            <h2 class="entry-title">
                                                <a href="news-details.html">Loud Africa</a>
                                            </h2>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post medium-post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/18.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            
                                            <h2 class="entry-title">
                                                <a href="news-details.html">Record Label</a>
                                            </h2>
                                        </div>
                                    </div><!--/post--> 
                                    
                                </div>
                            </div>
                        </div><!--/.section --> 
                        
                    </div><!--/.col-sm-9 -->    
                    
                    <div class="col-sm-3">
                        <div id="sitebar">                          
                            <div class="widget">
                                <h1 class="section-title title" style="text-align: center;"><a href="listing.html">Twitter feed</a></h1>
                             
                                  <a class="twitter-timeline" href="https://twitter.com/pablozvipclub?ref_src=twsrc%5Etfw">Tweets by pablozvipclub</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div><!--/#widget-->
                            
                            
                            <div class="widget">
                                <div class="add">
                                    <a href="#"><img class="img-responsive" src="images/post/add/add6.jpg" alt="" /></a>
                                </div>
                            </div>
                           
                        </div><!--/#sitebar-->
                    </div>
                </div>              
            </div><!--/.section-->
        </div><!--/.container-fluid--
                        