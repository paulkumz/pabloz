
        <div class="footer-top">
            <div class="container-fluid">
                <ul class="list-inline social-icons text-center">
                    <li><a href="#"><i class="fab fa-facebook-f"></i>Facebook</a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i>Twitter</a></li>
                    <li><a href="#"><i class="fab fa-instagram"></i>Instagram</a></li>
                    <li><a href="#"><i class="fab fa-youtube"></i>Youtube</a></li>
                </ul>
            </div>
        </div><!--/.footer-top-->
    
        <footer id="footer">
            <div class="bottom-widgets">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="widget">
                         <img class="main-logo img-responsive" style="width:100px;" src="images/logo1.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="widget">
                                <h2>Sites</h2> 
                                <ul>
                                    <li><a href="#">Gweru</a></li>
                                    <li><a href="#">Vic Falls</a></li>
                                    <li><a href="#">Harare</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="widget">
                                <h2>Popular</h2> 
                                <ul>
                                    <li><a href="#">Loud Africa</a></li>
                                    <li><a href="#">Pabloz Five</a></li>
                                    <li><a href="#">VIP Lounge</a></li>
                                    <li><a href="#">Kumusha</a></li>
                                    <li><a href="#">Music Auditions</a></li>
                                </ul>
                                
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="widget">
                                <h2>Partners</h2> 
                                <ul>
                                    <li><a href="#">Zambezi Magic</a></li>
                                    <li><a href="#">ZBC</a></li>
                                    <li><a href="#">Loud Africa</a></li>
                                    <li><a href="#">Star FM</a></li>                            
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container-fluid text-center">
                    <p>Copyright &copy; 2018,<a href="http://twitter.com/paulkumz"> Pabloz Productions</a></p>
                </div>
            </div>      
        </footer>   
    </div><!--/#main-wrapper--> 
    
        
    <!--/#scripts--> 
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="js/main.js"></script>