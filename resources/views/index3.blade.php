<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--title-->
    <title>Pabloz Productions | Media House</title>
    
    <!--CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/full-slider.css" rel="stylesheet">
     <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">   
    
    <!--Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Fjalla+One:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href=https://fonts.googleapis.com/css?family=Tangerine|Inconsolata|Droid+Sans rel='stylesheet' type='text/css'>

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <div id="main-wrapper"> 
       
        
        <header id="navigation">
            <div class="navbar" role="banner">  
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img class="main-logo img-responsive" src="images/logo1.png" alt="">
                        </a>
                    </div> 
                    <nav id="mainmenu" class="navbar-left collapse navbar-collapse"> 
                        <a class="secondary-logo" href="{{ url('/') }}">
                            <img class="img-responsive" src="images/logo.png" alt="">
                        </a>
                        <ul class="nav navbar-nav">                       
                            



<!--start the dropdown for each element-->
                            <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">VIP LOUNGE</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                     
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">CO-OP EVENTS</font></a>
                                                        </h2>
                                                         <h2>
                                                            <a href="news-details.html"><font color="black">VENUE HIRE</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">RESTAURANT</font></a>
                                                        </h2>
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/1.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">HARARE LOUNGE</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/2.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">VIC FALLS LOUNGE</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/3.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">GWERU LOUNGE</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <!--end of dropdown for each element-->

                                         <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">VISUAL STUDIO</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"></i><font color="black">VIDEO STUDIO</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"></i><font color="black">LOUD AFRICA</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"></i><font color="black">PABLOZ FIVE</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">BEHIND THE SCENES</font></a>
                                                        </h2>
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <iframe width="640" height="360" src="https://www.youtube.com/embed/csg3s6Ob4uc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Loud Africa Zimbabwe</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                          <iframe width="640" height="360" src="https://www.youtube.com/embed/6ql2hjFBsoo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">New competitions in the house</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                           <iframe width="640" height="360" src="https://www.youtube.com/embed/KoCh4dlzNPo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Drama in music</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                           

                           <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">MUSIC STUDIO</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"><font color="black">RECORDING SERVICES</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">PRODUCER</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">RECORD LABEL</font></a>
                                                        </h2>
                                                        
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/17.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">

                                                            <a href="news-details.html">Recording Studio</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/18.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Mixing and Mastering</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/20.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Record label  audition</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>


                           <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">CELEBRITY NEWS</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"><font color="black">LATEST NEWS</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">FASHION</font></a>
                                                        </h2>
                                                       
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/1.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">A wide range of cocktails? yeah you right!</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/2.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Friday nights are nolonger the same here! lets get grooving</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/3.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Why is the media so afraid of Facebook?</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">GALLERY</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"><font color="black">VIC FALLS LOUNGE</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">HARARE LOUNGE</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">GWERU LOUNGE</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">RESTAURANT</font></a>
                                                        </h2>
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/1.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">A wide range of cocktails? yeah you right!</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/2.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Friday nights are nolonger the same here! lets get grooving</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/3.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Why is the media so afraid of Facebook?</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">ABOUT</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"><font color="black">PABLOZ PRODUCTIONS</font></a>
                                                        </h2>
                                                       
                                                        <hr>
                                                        <div class="entry-meta">
                                                               <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/12.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">In patnership with zambezi magic</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/14.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">We offer the best lounge service in town</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/3.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Why is the media so afraid of Facebook?</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>                   
                    </nav>
                    
                    <div class="searchNlogin">
                        <ul>
                            <li class="search-icon"><i class="fa fa-search"></i></li>
                            <li class="dropdown user-panel"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i></a>
                                <div class="dropdown-menu top-user-section">
                                    <div class="top-user-form">
                                        <form id="top-login" role="form">
                                            <div class="input-group" id="top-login-username">
                                                <span class="input-group-addon"><img src="images/others/user-icon.png" alt="" /></span>
                                                <input type="text" class="form-control" placeholder="Username" required="">
                                            </div>
                                            <div class="input-group" id="top-login-password">
                                                <span class="input-group-addon"><img src="images/others/password-icon.png" alt="" /></span>
                                                <input type="password" class="form-control" placeholder="Password" required="">
                                            </div>
                                            <div>
                                                <p class="reset-user">Forgot <a href="#">Password/Username?</a></p>
                                                <button class="btn btn-danger" type="submit">Login</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="create-account">
                                        <a href="#">Create a New Account</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="search">
                            <form role="form">
                                <input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
                            </form>
                        </div> <!--/.search--> 
                    </div><!--.searchNlogin -->
                </div>  
            </div>
        </header><!--/#navigation-->





    <!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/slider/21.jpg');"></div>
                <div class="carousel-caption">
                    <div class="jumbotron" style="background:transparent">
  <h2>THE VIP LOUNGE,GWERU,HARARE<br> AND VIC FALLS</h2>
  <p>LETS GET GROOVING!</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">EXCLUSIVE</a></p>
</div>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/slider/2.jpg');"></div>
                <div class="carousel-caption">
                    <div class="jumbotron" style="background:transparent">
  <h2>WATCH PABLOZ FIVE <br>PREMIERE THIS WEEK</h2>
  <p>ITS AIRING ON 25/12</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">PREVIEW</a></p>
                </div>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/slider/3.jpg');"></div>
                <div class="carousel-caption">
                    <div class="jumbotron" style="background:transparent">
  <h2>LOUD AFRICA ZIMBABWE <br>SEASON FIVE </h2>
  <p>AIRING 22/12</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">PREVIEW</a></p>
                </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>

   
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>




       







 <div class="container-fluid">   
       
            <div class="section add inner-add">
                <a href="#"><img class="img-responsive" src="images/post/add/add2.jpg" alt="" /></a>
            </div><!--/.section-->      
            <div class="section">
                <div class="row">
                    <div class="col-sm-3">
                        <h2 class="section-title title" style="font-family: 'Fjalla One', sans-serif;">LATEST NEWS</h2> 
                        <div class="left-sidebar">
                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/10.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 13, 2018 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>200</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Young artist releases album under Pabloz records</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/11.png" alt="" /></a>
                                    </div>
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 11, 2018 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>21</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Pablos productions sweeps all awards</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/1.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>21</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Jamaican surtday dominates Harare...</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 

                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/4.jpg" alt="" /></a>
                                    </div>
                                    
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>21k</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Much anticipated drama series of the year out!</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                            
                          
                        </div><!--/left-sidebar-->  
                    </div>
                    
                    <div class="col-sm-6">
                        <div id="site-content" class="site-content">
                            <h1 class="section-title title" style="font-family: 'Fjalla One', sans-serif;"><a href="listing.html">EXCLUSIVE AND POPULAR CONTENTS</a></h1>
                            <div class="middle-content">
                                <div id="top-news" class="section top-news">
                                    <div class="post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/4.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            <div class="entry-meta">
                                              
                                            </div>
                                            <h2 class="entry-title" style="font-family: 'Fjalla One', sans-serif;">
                                                <a href="news-details.html">LOUD AFRICA SEASON 4 PREMIERE</a>
                                            </h2>
                                            <div class="entry-content">
                                                <p>Text of the printing and typesetting industry orem Ipsum has been the industry standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book ......</p>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/5.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            <div class="entry-meta">
                                                
                                            </div>
                                            <h2 class="entry-title">
                                                <a href="news-details.html">PABLOZ FIVE SEASON 2 PREMIERE</a>
                                            </h2>
                                            <div class="entry-content">
                                                <p>Text of the printing and typesetting industry orem Ipsum has been the industry standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book ......</p>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/2.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            <div class="entry-meta">
                                                
                                            </div>
                                            <h2 class="entry-title">
                                                <a href="news-details.html">MUSIC AUDITIONS BEHIND THE SCENCES </a>
                                            </h2>
                                            <div class="entry-content">
                                                <p>Text of the printing and typesetting industry orem Ipsum has been the industry standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book ......</p>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                </div><!-- top-news -->
                                
                                <div class="section health-section">
                                    <h1 class="section-title"><a href="listing.html">LATEST IN MUSIC AND SHOWS</a></h1>
                                    <div class="health-feature">
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <iframe width="640" height="360" src="https://www.youtube.com/embed/upUEP7uGmsg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"  allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 20, 2018 </a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Watch two upcoming artists rock the stage at the pabloz vic falls carnival</a>
                                                </h2>
                                            </div>
                                        </div><!--/post--> 
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/3.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 23, 2018 </a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">It has never been easier to produce music </a>
                                                </h2>
                                            </div>
                                        </div><!--/post--> 
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/12.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 6, 2018 </a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">All pabloz shows now airing on zambezi magic</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </div>
                                </div><!--/.health-section -->
                                
                                <div class="add inner-add">
                                   
                                </div><!--/.section-->
                              
                                
                       
                            </div><!--/.middle-content-->
                        </div><!--/#site-content-->
                    </div>
                    <div class="col-sm-3">
                        <div id="sitebar">                          
                            <div class="widget">
                                <h1 class="section-title title" style="font-family: 'Fjalla One', sans-serif;"><a href="listing.html">TRENDING</a></h1>
                                <ul class="post-list">
                                   
                                    <li>
                                        <div class="post small-post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/15.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="video-catagory"><a href="#">VIP Lounge</a></div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">2 Great DJ's land at pabloz on a thursday</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/14.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="video-catagory"><a href="#">Lifestyle</a></div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Celebrity interview of the month</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                   
                                    <li>
                                        <div class="post small-post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/16.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="video-catagory"><a href="#">Lifestyle</a></div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Pabloz Gweru explosive all white night</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                </ul>
                            </div><!--/#widget-->
                            <div class="widget">
                                <h2 class="section-title">Upcoming Events</h2>
                                <ul class="comment-list">
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">  
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Nov 30, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">All white saturday at pabloz Harare</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">  
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Nov 28, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Jamaican birthday bash</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">  
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                       <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Nov 23, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Premiere of pabloz 5 show</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Dec 15, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">African cultural carnival</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Dec 24, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Christmass count up Monday</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                </ul>
                            </div><!-- widget -->
                            
                            <div class="widget">
                                <div class="add">
                                    <a href="#"><img class="img-responsive" src="images/post/add/add5.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="widget">
                                <div class="post video-post medium-post">
                                    <div class="entry-header">
                                        <div class="entry-thumbnail embed-responsive embed-responsive-16by9">
                                          <iframe width="640" height="360" src="https://www.youtube.com/embed/5HdcKZMiolo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <div class="post-content">                              
                                        <div class="video-catagory"><a href="#">Music</a></div>
                                        <h2 class="entry-title">
                                            <a href="news-details.html">Loud Africa behind the scenes</a>
                                        </h2>
                                    </div>
                                </div><!--/post-->
                                
                            </div>
                        </div><!--/#sitebar-->
                    </div>
                </div>              
            </div><!--/.section-->

        </div><!--/.container-fluid-->
        
        <div class="footer-top">
            <div class="container-fluid">
                <ul class="list-inline social-icons text-center">
                    <li><a href="#"><i class="fab fa-facebook-f"></i>Facebook</a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i>Twitter</a></li>
                    <li><a href="#"><i class="fab fa-instagram"></i>Instagram</a></li>
                    <li><a href="#"><i class="fab fa-youtube"></i>Youtube</a></li>
                </ul>
            </div>
        </div><!--/.footer-top-->
    
        <footer id="footer">
            <div class="bottom-widgets">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="widget">
                         <img class="main-logo img-responsive" style="width:100px;" src="images/logo1.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="widget">
                                <h2>Sites</h2> 
                                <ul>
                                    <li><a href="#">Gweru</a></li>
                                    <li><a href="#">Vic Falls</a></li>
                                    <li><a href="#">Harare</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="widget">
                                <h2>Popular</h2> 
                                <ul>
                                    <li><a href="#">Loud Africa</a></li>
                                    <li><a href="#">Pabloz Five</a></li>
                                    <li><a href="#">VIP Lounge</a></li>
                                    <li><a href="#">Kumusha</a></li>
                                    <li><a href="#">Music Auditions</a></li>
                                </ul>
                                
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="widget">
                                <h2>Partners</h2> 
                                <ul>
                                    <li><a href="#">Zambezi Magic</a></li>
                                    <li><a href="#">ZBC</a></li>
                                    <li><a href="#">Loud Africa</a></li>
                                    <li><a href="#">Star FM</a></li>                            
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container-fluid text-center">
                    <p>Copyright &copy; 2018,<a href="http://twitter.com/paulkumz"> Pabloz Productions</a></p>
                </div>
            </div>      
        </footer>   
    </div><!--/#main-wrapper--> 
    
        
    <!--/#scripts--> 
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="js/main.js"></script>
    
</body>
</html>